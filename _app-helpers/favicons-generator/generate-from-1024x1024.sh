#!/bin/bash
#favicons generator
# .ico
convert 1024x1024.png -resize 16x16^ -gravity center -extent 16x16 -flatten -colors 256 -background transparent ../../public/favicon.ico
convert 1024x1024.png -resize 32x32^ -gravity center -extent 32x32 -flatten -colors 256 -background transparent ../../public/favicon-32x32.ico
convert 1024x1024.png -resize 48x48^ -gravity center -extent 48x48 -flatten -colors 256 -background transparent ../../public/favicon-48x48.ico
# .png
convert 1024x1024.png -resize 48x48^  -gravity center -extent 48x48 ../../public/img/icons/android-chrome-48x48.png
convert 1024x1024.png -resize 72x72^  -gravity center -extent 72x72 ../../public/img/icons/android-chrome-72x72.png
convert 1024x1024.png -resize 96x96^  -gravity center -extent 96x96 ../../public/img/icons/android-chrome-96x96.png
convert 1024x1024.png -resize 128x128^  -gravity center -extent 128x128 ../../public/img/icons/android-chrome-128x128.png
convert 1024x1024.png -resize 144x144^  -gravity center -extent 144x144 ../../public/img/icons/android-chrome-144x144.png
convert 1024x1024.png -resize 192x192^  -gravity center -extent 192x192 ../../public/img/icons/android-chrome-192x192.png
convert 1024x1024.png -resize 256x256^  -gravity center -extent 256x256 ../../public/img/icons/android-chrome-256x256.png
convert 1024x1024.png -resize 512x512^  -gravity center -extent 512x512 ../../public/img/icons/android-chrome-512x512.png
convert 1024x1024.png -resize 180x180^  -gravity center -extent 180x180 ../../public/apple-touch-icon.png
convert 1024x1024.png -resize 57x57^  -gravity center -extent 57x57 ../../public/img/icons/apple-touch-icon-57x57.png
convert 1024x1024.png -resize 60x60^  -gravity center -extent 60x60 ../../public/img/icons/apple-touch-icon-60x60.png
convert 1024x1024.png -resize 76x76^  -gravity center -extent 76x76 ../../public/img/icons/apple-touch-icon-76x76.png
convert 1024x1024.png -resize 114x114^  -gravity center -extent 114x114 ../../public/img/icons/apple-touch-icon-114x114.png
convert 1024x1024.png -resize 120x120^  -gravity center -extent 120x120 ../../public/img/icons/apple-touch-icon-120x120.png
convert 1024x1024.png -resize 152x152^  -gravity center -extent 152x152 ../../public/img/icons/apple-touch-icon-152x152.png
convert 1024x1024.png -resize 168x168^  -gravity center -extent 168x168 ../../public/img/icons/apple-touch-icon-168x168.png
convert 1024x1024.png -resize 180x180^  -gravity center -extent 180x180 ../../public/img/icons/apple-touch-icon.png
convert 1024x1024.png -resize 180x180^  -gravity center -extent 180x180 ../../public/img/icons/apple-touch-icon-180x180.png
convert 1024x1024.png -resize 16x16^  -gravity center -extent 16x16 ../../public/img/icons/favicon-16x16.png
convert 1024x1024.png -resize 32x32^  -gravity center -extent 32x32 ../../public/img/icons/favicon-32x32.png
convert 1024x1024.png -resize 70x70^  -gravity center -extent 70x70 ../../public/img/icons/mstile-70x70.png
convert 1024x1024.png -resize 144x144^  -gravity center -extent 144x144 ../../public/img/icons/msapplication-icon-144x144.png
convert 1024x1024.png -resize 150x150^  -gravity center -extent 150x150 ../../public/img/icons/mstile-150x150.png
convert 1024x1024.png -resize 310x150^  -gravity center -extent 310x150 ../../public/img/icons/mstile-310x150.png
convert 1024x1024.png -resize 310x310^  -gravity center -extent 310x310 ../../public/img/icons/mstile-310x310.png