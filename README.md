# VueJS frontend

example https://test-vue.egyptian-pharaoh.xyz/

pwa app(can be install on android device as an application)

has no cart logic.

for logic, see

-   https://vue-examples.egyptian-pharaoh.xyz/compare/
-   https://vue-examples.egyptian-pharaoh.xyz/same/
-   https://vue-examples.egyptian-pharaoh.xyz/jack/
-   https://vue-examples.egyptian-pharaoh.xyz/hex/

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Run your tests

```
npm run test
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
