import Vue from 'vue';
import Router from 'vue-router';
import Loader from './views/Loader.vue';

Vue.use(Router);

export default new Router({
	// mode: 'history',
	base: process.env.BASE_URL,
	routes: [
		{
			path: '*',
			name: 'Loader',
			component: Loader,
		},
		{
			path: '/',
			name: 'Home',
			// route level code-splitting
			// this generates a separate chunk (about.[hash].js) for this route
			// which is lazy-loaded when the route is visited.
			component: () => import(/* webpackChunkName: "home" */ './views/Home.vue'),
		},
	],
});
