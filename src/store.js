import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		// ak@ts
		appInfo: {
			name: process.env.VUE_APP_TITLE,
			domain: process.env.VUE_APP_DOMAIN,
			company: process.env.VUE_APP_COMPANY,
			email: process.env.VUE_APP_EMAIL,
			address: process.env.VUE_APP_ADRESS,
		},

		// not used here
		appData: {},
		appUI: {
			sound: false,
			// offline: false
		},
		// not used here/
		// ak@ts/

		gameObjects: [],
	},
	getters: {
		getAppInfo: (state) => {
			return state.appInfo;
		},
		getAppData: (state) => {
			return state.appData;
		},
		getAppUI: (state) => {
			return state.appUI;
		},
		getGameObjects: (state) => {
			return state.gameObjects;
		},
	},
	mutations: {
		setAppInfo(state, objProp) {
			let objKeys = Object.keys(objProp);
			objKeys.forEach((element) => {
				state.appInfo[element] = objProp[element];
			});
		},
		setAppData(state, objProp) {
			let objKeys = Object.keys(objProp);
			objKeys.forEach((element) => {
				state.appData[element] = objProp[element];
			});
		},
		setAppUI(state, objProp) {
			let objKeys = Object.keys(objProp);
			objKeys.forEach((element) => {
				state.appUI[element] = objProp[element];
			});
		},
		setGameObjects(state, objects) {
			state.gameObjects = objects;
		},
	},
	actions: {},
});
