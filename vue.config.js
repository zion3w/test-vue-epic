// const path = require("path");
process.env.VUE_APP_VERSION = require('./package.json').version;
process.env.VUE_APP_LANG = 'en';
process.env.VUE_APP_TITLE = 'VUE_APP_TITLE';
process.env.VUE_APP_DESCRIPTION = 'VUE_APP_DESCRIPTION';
process.env.VUE_APP_KEYWORDS = 'VUE_APP_KEYWORDS';
process.env.VUE_APP_DOMAIN = 'VUE_APP_DOMAIN';
// appInfo
process.env.VUE_APP_COMPANY = 'VUE_APP_COMPANY';
process.env.VUE_APP_TWITTER_USERNAME = '@VUE_APP_TWITTER_USERNAME';
process.env.VUE_APP_EMAIL = 'VUE_APP_EMAIL@test.com';
process.env.VUE_APP_ADRESS = 'VUE_APP_ADRESS';
// appInfo/
process.env.VUE_APP_THEME_COLOR = '#000000';

module.exports = {
	// publicPath: process.env.NODE_ENV === 'production' ? '/production-sub-path/' : '/',
	publicPath: './',
	productionSourceMap: false,
	css: {
		// sourceMap: true
		// sourceMap: false
		sourceMap: process.env.NODE_ENV === 'production' ? false : true,
	},

	pwa: {
		// https://github.com/vuejs/vue-cli/tree/dev/packages/%40vue/cli-plugin-pwa
		name: process.env.VUE_APP_TITLE,
		themeColor: process.env.VUE_APP_THEME_COLOR,
		msTileColor: process.env.VUE_APP_THEME_COLOR,
		appleMobileWebAppCapable: 'yes',
		appleMobileWebAppStatusBarStyle: 'black',

		// // configure the workbox plugin
		// workboxPluginMode: 'InjectManifest',
		// // workboxPluginMode: "GenerateSW",
		// workboxOptions: {
		// 	// swSrc is required in InjectManifest mode.
		// 	swSrc: 'public/service-worker.js',
		// 	// swDest is required in GenerateSW mode.
		// 	// swDest: "public/service-worker.js"
		// 	// ...other Workbox options...
		// },
	},
	devServer: {
		// contentBase: path.join(__dirname, 'dist'),
		// compress: true,
		port: 9000,
		// clientLogLevel: 'silent'
		// host: '0.0.0.0',
		// host: 'vuedev'
		// open: true
		// open: 'Google Chrome'
	},
	chainWebpack: (config) => {
		// clear the existing images module
		const imagesRule = config.module.rule('images');
		imagesRule.uses.clear();

		imagesRule
			// you can set whatever you want or remove this line completely
			.test(/\.(png|jpe?g|gif|webp)(\?.*)?$/)
			.use('file-loader')
			.loader('file-loader')
			.tap((options) => {
				return {
					...options,
					// limit: -1 // no limit
					limit: 1000, // 1kb limit
				};
			})
			.end();
	},
};
